### poseidonFB ###

Welcome to poseidonFB, is a freeBASIC IDE written in the D language utilizing IUP framework.

It supports:

1. Multiple platforms (Windows, Linux)
2. Syntax highlighting
3. Project manager
4. Autocompletion & Calltip
5. Jump to defintion
5. Function / Type / Variable... treeview
7. Find / Replace in document or project
8. Utf-8/16/32 decode & encode
9. Debug
10. Compile / Quick run / Build project
11. Self-define short cuts
12. etc...

Win7:

![poseidonFB_windows.png](https://bitbucket.org/repo/j5rjj4/images/501719893-poseidonFB_windows.png)

Linux mint18 x64:

![linux64.png](https://bitbucket.org/repo/j5rjj4/images/559348986-linux64.png)

## Install ##
Go to **Downloads** https://bitbucket.org/KuanHsu/poseidonfb/downloads/ to select file to download.

**Win:**

Download poseidonFB.7z, extract to folder, then run the poseidonFB.exe

**Linux:**

First install IUP framework(3.22), https://sourceforge.net/projects/iup/files/3.22/Linux%20Libraries/

Open the terminal to install the *.so to system.

```
#!d

sudo ./install

```

Then choose x64 or x86 version to download, extract to folder, then run the poseidonFB